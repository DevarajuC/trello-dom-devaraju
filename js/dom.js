//GET
const getCard = () =>
    fetch(
        `https://api.trello.com/1/cards/5e858de109a0e93e17cdf586?key=29d3c0b5e4d2bc3c5038057e5704229a&token=e5381e778dca0e87ba4704ce086857de956f6ab8793188a6422418f7ab2cf703`, {
            method: "GET",
            headers: {
                Accept: "application/json"
            }
        }
    )
    .then(response => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
    })
    .then(text => console.log(text))
    .catch(error => console.error(error));

//CREATE CARD
const createCard = () => {
    fetch(
            `https://api.trello.com/1/cards?name=trello-to-do&idList=5e858a8016a7b13e973ba6f2&key=29d3c0b5e4d2bc3c5038057e5704229a&token=e5381e778dca0e87ba4704ce086857de956f6ab8793188a6422418f7ab2cf703`, {
                method: "POST",
                headers: {
                    Accept: "application/json"
                }
            }
        )
        .then(response => {
            console.log(`Response: ${response.status} ${response.statusText}`);
            return response.text();
        })
        .then(text => console.log(text))
        .catch(error => console.error(error));
};

//CREATE DIV
const createDiv = () => {
    var newDiv = document.createElement("div");
    newDiv.classList.add("card");
    newDiv.addEventListener("click", createCard);
    document.getElementById("card-all").append(newDiv);
};

console.log(createDiv);